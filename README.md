﻿Definately spent the full 14 hours.  Most everything is implemented with the exception of a couple power ups.  I only implemented more bombs and remote detonation.

The code is not commented as well as I would like, and I also coded more for speed than anything as it was a lot to get done in a short period of time.  I used mostly C++ with a smattering of blueprint where it made sense (UI, archetypes, and minor events)

I also implemented the optional dynamic camera (though the code is hacky), as well as the procedural level (again hacky).

I will also note that I have never used git before, so there was/is a bit of a learning curve there.  I have been using Perforce for 15+ years.

Controls:  Player 1 - WASD for movement, spacebar drops a bomb (must be one tile away)
	   Player 2 - Arrow keys for movement, Numpad 0 for bomb