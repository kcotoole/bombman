// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BomberManLevel.h"
#include "UserWidget.h"
#include "BomberManGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BOMBERMAN_API ABomberManGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	ABomberManGameModeBase();

	void EndRound();

	UFUNCTION(BlueprintCallable, Category = "Round")
	FString GetRoundWinnerString() { return RoundWinnerString; }

protected:
	void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	
	void Tick(float DeltaTime) override;

	bool RoundShouldEnd();

public:

	UPROPERTY(EditAnywhere)
	TSubclassOf<UUserWidget> EndRoundWidgetClass;

	UPROPERTY()
	UUserWidget *EndRoundWidget;

	// The type of level we'll create
	UPROPERTY(EditAnywhere)
	TSubclassOf<ABomberManLevel> BomberManLevelClass;

	UPROPERTY()
	ABomberManLevel *Level;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float RoundTime;

	FString RoundWinnerString;
};
