// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberManBomb.h"
#include "BomberManLevel.h"
#include "BomberManGameModeBase.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ABomberManBomb::ABomberManBomb()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TimerLength = 3.0f;

	BombMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BombMesh"));
	RootComponent = BombMeshComp;
}

// Called when the game starts or when spawned
void ABomberManBomb::BeginPlay()
{
	Super::BeginPlay();
	
	GetWorld()->GetTimerManager().SetTimer(ExplodeTimerHandle, this, &ABomberManBomb::Explode, TimerLength, false);
}

void ABomberManBomb::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	GetWorld()->GetTimerManager().ClearTimer(ExplodeTimerHandle);

}

// Called every frame
void ABomberManBomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABomberManBomb::Explode()
{
// 	FHitResult HitResult;
// 	FCollisionQueryParams QueryParams;
// 	QueryParams.AddIgnoredActor(this);
// 	
// 	bool bHit = GetWorld()->LineTraceSingleByChannel(HitResult, vStart, TileCenter, ECollisionChannel::ECC_WorldDynamic, QueryParams);

	ExplodeInDirection(FVector(1 ,0, 0), TILESIZE*3);
	ExplodeInDirection(FVector(-1, 0, 0), TILESIZE * 3);
	ExplodeInDirection(FVector(0, 1, 0), TILESIZE * 3);
	ExplodeInDirection(FVector(0, -1, 0), TILESIZE * 3);

	OnBombExplode.Broadcast(this);

	OnExplode();
	Destroy();
}

void ABomberManBomb::ExplodeInDirection(FVector Dir, float Length)
{
	FHitResult HitResult;
	FCollisionQueryParams QueryParams;
	QueryParams.AddIgnoredActor(this);

	FVector Start = GetActorLocation();
	FVector End = Start + Dir*Length;

	bool bHit = GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECC_Bomb, QueryParams);

	ABomberManGameModeBase *GameMode = Cast<ABomberManGameModeBase>(UGameplayStatics::GetGameMode(this));
	ABomberManLevel *Level = GameMode->Level;
	FVector HitLoc;
	if (bHit)
	{
		 HitLoc = HitResult.GetActor()->GetActorLocation();
	}
	else
	{
		HitLoc = End;
	}

	int TileHitX, TileHitY;
	Level->WorldCoordsToTileCoords(HitLoc, TileHitX, TileHitY);

	int BombTileX, BombTileY;
	Level->WorldCoordsToTileCoords(GetActorLocation(), BombTileX, BombTileY);

	FTransform XForm = FTransform::Identity;

	if (BombTileX != TileHitX)
	{
		for (int x = BombTileX; x != TileHitX; x += FMath::Sign(TileHitX - BombTileX))
		{
			FVector ParticleLoc;
			Level->TileCoordsToWorldCoords(x, BombTileY, ParticleLoc);
			ParticleLoc.Z += 50.0f;
			XForm.SetLocation(ParticleLoc);
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionPSys, XForm);
		}
	}
	else if (BombTileY != TileHitY)
	{
		for (int y = BombTileY; y != TileHitY; y += FMath::Sign(TileHitY - BombTileY))
		{
			FVector ParticleLoc;
			Level->TileCoordsToWorldCoords(BombTileX, y, ParticleLoc);
			ParticleLoc.Z += 50.0f;
			XForm.SetLocation(ParticleLoc);
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionPSys, XForm);
		}
	}

	if (bHit)
	{
		HitResult.GetActor()->TakeDamage(1000.0f, FDamageEvent(), NULL, this);
	}
}

