// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BomberManPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class BOMBERMAN_API ABomberManPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ABomberManPlayerController();
	
	void BeginPlay() override;

	// Called to bind functionality to input
	virtual void SetupInputComponent() override;

	int GetPlayerIndex();
};
