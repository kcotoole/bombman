// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberManCamera.h"
#include "BomberManPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "BomberManUnit.h"

// Sets default values
ABomberManCamera::ABomberManCamera()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));

	// Make the scene component the root component
	RootComponent = SceneComponent;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
	SpringArmComponent->SetupAttachment(SceneComponent);

	// Setup camera defaults
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->FieldOfView = 90.0f;
	CameraComponent->bConstrainAspectRatio = true;
	CameraComponent->AspectRatio = 1.777778f;
	CameraComponent->PostProcessBlendWeight = 1.0f;
	CameraComponent->SetupAttachment(SpringArmComponent);
}

// Called when the game starts or when spawned
void ABomberManCamera::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ABomberManCamera::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	APlayerController *PC1 = UGameplayStatics::GetPlayerController(this, 0);
	APlayerController *PC2 = UGameplayStatics::GetPlayerController(this, 1);

	if (!PC1 || !PC2)
	{
		// Something is wrong, we don't have a player controller for one of the players
		// Fail gracefully
		return;
	}

	ABomberManUnit *Unit_P1 = Cast<ABomberManUnit>(PC1->GetPawn());
	ABomberManUnit *Unit_P2 = Cast<ABomberManUnit>(PC2->GetPawn());

	if (!Unit_P1 || !Unit_P2)
	{
		// Something is wrong, we don't have a player pawn for one of the players
		// Fail gracefully
		return;
	}

	// Pretty hacky dynamic camera code, Shorten the spring arm length based on distance
	FVector Diff = Unit_P1->GetActorLocation() - Unit_P2->GetActorLocation();
	float Distance = Diff.Size()*1.25f;
	float ArmLengthTarget = FMath::Clamp(Distance, 750.0f, 1500.0f);
	SpringArmComponent->TargetArmLength += (ArmLengthTarget - SpringArmComponent->TargetArmLength)*DeltaTime;

	// Have the camera always interpolating to the average location of the pawns
	FVector OldCamLoc = GetActorLocation();
	FVector TargetCamLoc = (Unit_P1->GetActorLocation() + Unit_P2->GetActorLocation()) / 2.0f;
	FVector NewCamLoc = OldCamLoc + (TargetCamLoc - OldCamLoc)*DeltaTime*2.0f;
	SetActorLocation(NewCamLoc);
}

