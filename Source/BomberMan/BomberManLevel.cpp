// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberManLevel.h"


// Sets default values
ABomberManLevel::ABomberManLevel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABomberManLevel::BeginPlay()
{
	Super::BeginPlay();
	
	SpawnBorderWalls();
	SpawnInteriorWalls();
}

// Called every frame
void ABomberManLevel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABomberManLevel::WorldCoordsToTileCoords(FVector InWorldCoord, int &OutX, int &OutY)
{
	OutX = (InWorldCoord.X + TILESIZE/2 + (TILESIZE*TILES_X/2)) / TILESIZE;
	OutY = (InWorldCoord.Y + TILESIZE/2 + (TILESIZE*TILES_Y/2)) / TILESIZE;
}

void ABomberManLevel::TileCoordsToWorldCoords(int InX, int InY, FVector &OutWorldCoord)
{
	OutWorldCoord.X = InX*TILESIZE - (TILESIZE*TILES_X / 2);
	OutWorldCoord.Y = InY*TILESIZE - (TILESIZE*TILES_Y / 2);
	OutWorldCoord.Z = 0.0f;
}

void ABomberManLevel::SpawnBorderWalls()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.bNoFail = true;

	FTransform XForm;
	FVector WorldPos;
	for (int x = 0; x < TILES_X; x++)
	{
		TileCoordsToWorldCoords(x, 0, WorldPos);
		WorldPos.Z += TILESIZE / 2; // Our wall height is also our tilesize, bump up by half for correct placement
		XForm.SetLocation(WorldPos);
		GetWorld()->SpawnActor<ABomberManWall>(IndestructibleWallClass, XForm, SpawnParams);

		TileCoordsToWorldCoords(x, TILES_Y-1, WorldPos);
		WorldPos.Z += TILESIZE / 2; // Our wall height is also our tilesize, bump up by half for correct placement
		XForm.SetLocation(WorldPos);
		GetWorld()->SpawnActor<ABomberManWall>(IndestructibleWallClass, XForm, SpawnParams);
	}

	for (int y = 1; y < TILES_Y-1; y++)
	{
		TileCoordsToWorldCoords(0, y, WorldPos);
		WorldPos.Z += TILESIZE / 2; // Our wall height is also our tilesize, bump up by half for correct placement
		XForm.SetLocation(WorldPos);
		GetWorld()->SpawnActor<ABomberManWall>(IndestructibleWallClass, XForm, SpawnParams);

		TileCoordsToWorldCoords(TILES_X-1, y, WorldPos);
		WorldPos.Z += TILESIZE / 2; // Our wall height is also our tilesize, bump up by half for correct placement
		XForm.SetLocation(WorldPos);
		GetWorld()->SpawnActor<ABomberManWall>(IndestructibleWallClass, XForm, SpawnParams);
	}
}

void ABomberManLevel::SpawnInteriorWalls()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.bNoFail = true;

	FTransform XForm;
	FVector WorldPos;

	for (int x = 1; x < TILES_X - 1; x++)
	{
		for (int y = 1; y < TILES_Y - 1; y++)
		{
			if (TileCanBeIndestructible(x, y))
			{
				TileCoordsToWorldCoords(x, y, WorldPos);
				WorldPos.Z += TILESIZE / 2; // Our wall height is also our tilesize, bump up by half for correct placement
				XForm.SetLocation(WorldPos);

				if (rand() % 100 > 25)
				{
					GetWorld()->SpawnActor<ABomberManWall>(IndestructibleWallClass, XForm, SpawnParams);
				}
				else if (TileCanBeDestructible(x,y))
				{
					GetWorld()->SpawnActor<ABomberManWall>(DestructibleWallClass, XForm, SpawnParams);
				}
			}
			else if (TileCanBeDestructible(x, y))
			{
				TileCoordsToWorldCoords(x, y, WorldPos);
				WorldPos.Z += TILESIZE / 2; // Our wall height is also our tilesize, bump up by half for correct placement
				XForm.SetLocation(WorldPos);

				if (rand() % 100 > 75)
				{
					GetWorld()->SpawnActor<ABomberManWall>(DestructibleWallClass, XForm, SpawnParams);
				}
			}
		}
	}
}

bool ABomberManLevel::TileCanBeIndestructible(int x, int y)
{
	if (x <= 2 && y <= 2 || x >= TILES_X - 3 && y >= TILES_X - 3)
		return false;

	if (x % 2 && y % 2)
		return true;

	return false;
}

bool ABomberManLevel::TileCanBeDestructible(int x, int y)
{
	if (x <= 2 && y <= 2 || x >= TILES_X - 3 && y >= TILES_X - 3)
		return false;

	if (x % 2 && y % 2)
		return true;

	return true;
}