// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BomberManBomb.h"
#include "BomberManUnit.generated.h"


UCLASS()
class BOMBERMAN_API ABomberManUnit : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABomberManUnit();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void HandleMovementInput();

	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;	

	void InputMoveLeftPressed();
	void InputMoveRightPressed();
	void InputMoveUpPressed();
	void InputMoveDownPressed();

	void InputMoveLeftReleased();
	void InputMoveRightReleased();
	void InputMoveUpReleased();
	void InputMoveDownReleased();

	void InputBomb();

	bool IsDead() { return bDead; }

	int GetMaxBombs();

	void ApplyPowerUp(class ABomberManPickup* Pickup);

	UFUNCTION()
	void OnPowerUpExpired(class ABomberManPickup* Pickup);

	UFUNCTION(BlueprintCallable, Category = PowerUp)
	void AddRemoteDetonator() { RemoteDetonatorCount++; }

	UFUNCTION(BlueprintCallable, Category = PowerUp)
	void RemoveRemoteDetonator() { RemoteDetonatorCount--; }

	UFUNCTION(BlueprintCallable, Category = PowerUp)
	void AddMaxBombs() { MaxBombCount++; }

	UFUNCTION(BlueprintCallable, Category = PowerUp)
	void RemoveMaxBombs() { MaxBombCount--; }

	UFUNCTION()
	void OnBombExplode(ABomberManBomb *ExplodedBomb);

protected:
	bool bMoveLeft;
	bool bMoveRight;
	bool bMoveUp;
	bool bMoveDown;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Bomb)
	TSubclassOf<ABomberManBomb> BombClass;

	bool bDead;

	int RemoteDetonatorCount; // How many remote detonator powerups have been applied
	int MaxBombCount;

	TArray<ABomberManBomb *> Bombs;
};
