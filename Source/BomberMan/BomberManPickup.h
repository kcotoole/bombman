// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "BomberManUnit.h"
#include "BomberManPickup.generated.h"

// Delegate signature for a powerup expiring
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPowerUpExpiredSignature, class ABomberManPickup*, Pickup);

// Class is abstract to ensure we can't create it without subclassing
UCLASS(Abstract)
class BOMBERMAN_API ABomberManPickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomberManPickup();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Implement me in sub classes - I'll implement this in blueprint for ease, Made a NativeEvent so it could be implemented in Native or overriden in blueprint
	UFUNCTION(BlueprintNativeEvent)
	void ApplyEffect(ABomberManUnit *Unit);

	// Implement me in sub classes - I'll implement this in blueprint for ease, Made a NativeEvent so it could be implemented in Native or overriden in blueprint
	UFUNCTION(BlueprintNativeEvent)
	void RemoveEffect(ABomberManUnit *Unit);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	void PickUp(ABomberManUnit *PickupByUnit);

	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

protected:

	// Broadcast when the powerup has expired
	FPowerUpExpiredSignature OnPowerUpExpired;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pickup)
	UBoxComponent *BoxComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pickup)
	UStaticMeshComponent *MeshComp;

	bool bPickedUp;

	float PowerUpTimeLeft;
};
