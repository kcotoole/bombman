// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameViewportClient.h"
#include "BomberManGameViewportClient.generated.h"

/**
 * 
 */
UCLASS()
class BOMBERMAN_API UBomberManGameViewportClient : public UGameViewportClient
{
	GENERATED_BODY()
	
	// Unreals handling of the keyboard defaults to only one controller.  To handle 2 players on keyboard we need to override this function
	// NOTE: This code was pulled from here - https://wiki.unrealengine.com/Local_Multiplayer_Tips
	bool InputKey(FViewport* Viewport, int32 ControllerId, FKey Key, EInputEvent EventType, float AmountDepressed = 1.f, bool bGamepad = false) override;
	
};
