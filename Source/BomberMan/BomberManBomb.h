// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BomberManBomb.generated.h"

#define ECC_Bomb ECC_GameTraceChannel1

// Delegate signature for a powerup expiring
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBombExplodedSignature, class ABomberManBomb*, Bomb);

UCLASS()
class BOMBERMAN_API ABomberManBomb : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomberManBomb();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	void ExplodeInDirection(FVector Dir, float Length);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Explode();

	UFUNCTION(BlueprintImplementableEvent, Category = Bomb)
	void OnExplode();
	
	FBombExplodedSignature OnBombExplode;

protected:
	float TimerLength;

	FTimerHandle ExplodeTimerHandle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Bomb)
	UStaticMeshComponent *BombMeshComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Bomb)
	UParticleSystem *ExplosionPSys;
};
