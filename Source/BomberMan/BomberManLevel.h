// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BomberManWall.h"
#include "BomberManLevel.generated.h"

#define TILESIZE (100.0f)
#define TILES_X	(15)
#define TILES_Y	(15)

UCLASS()
class BOMBERMAN_API ABomberManLevel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomberManLevel();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void SpawnBorderWalls();
	void SpawnInteriorWalls();

	bool TileCanBeIndestructible(int x, int y);
	bool TileCanBeDestructible(int x, int y);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void WorldCoordsToTileCoords(FVector InWorldCoord, int &OutX, int &OutY);

	void TileCoordsToWorldCoords(int InX, int InY, FVector &OutWorldCoord);
	
public:
	UPROPERTY(EditAnywhere)
	TSubclassOf<ABomberManWall> IndestructibleWallClass;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ABomberManWall> DestructibleWallClass;
};
