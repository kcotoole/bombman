// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberManWall.h"
#include "BomberManLevel.h"

// Sets default values
ABomberManWall::ABomberManWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	BoxComp->SetBoxExtent(FVector(TILESIZE / 2, TILESIZE / 2, TILESIZE / 2), false);
	RootComponent = BoxComp;

	DestructibleComp = CreateDefaultSubobject<UDestructibleComponent>(TEXT("Destructible"));
	DestructibleComp->SetupAttachment(BoxComp);

	bDestroyed = false;
}

// Called when the game starts or when spawned
void ABomberManWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABomberManWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

float ABomberManWall::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	if (bDestructible && !bDestroyed)
	{
		DestructibleComp->SetCollisionProfileName(FName("NoBombCollision"));
		BoxComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		FVector ImpulseDir = GetActorLocation() - DamageCauser->GetActorLocation();
		ImpulseDir.Normalize();
		DestructibleComp->ApplyDamage(DamageAmount, DamageCauser->GetActorLocation(), ImpulseDir, 1000);

		

		SpawnPickup();

		bDestroyed = true;
	}

	return DamageAmount;
}

void ABomberManWall::SpawnPickup()
{
	if (rand() % 100 > 70)
	{
		int PickupIndex = rand() % PickupClasses.Num();

		FActorSpawnParameters SpawnParams;
		SpawnParams.bNoFail = true;
		FTransform XForm;

		XForm.SetLocation(GetActorLocation());
		GetWorld()->SpawnActor<ABomberManPickup>(PickupClasses[PickupIndex], XForm, SpawnParams);
	}

}
