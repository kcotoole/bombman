// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberManUnit.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BomberManGameModeBase.h"
#include "BomberManPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#include "Components/CapsuleComponent.h"
#include "BomberManPickup.h"

// Sets default values
ABomberManUnit::ABomberManUnit()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bDead = false;
	MaxBombCount = 1;
	RemoteDetonatorCount = 0;
}

// Called when the game starts or when spawned
void ABomberManUnit::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABomberManUnit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	HandleMovementInput();

}

// Called to bind functionality to input
void ABomberManUnit::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	ABomberManPlayerController  *PC = Cast<ABomberManPlayerController>(GetController());

	if (PC)
	{
		if (PC->GetPlayerIndex() == 0)
		{
			PlayerInputComponent->BindAction("P1_Left", EInputEvent::IE_Pressed, this, &ABomberManUnit::InputMoveLeftPressed);
			PlayerInputComponent->BindAction("P1_Right", EInputEvent::IE_Pressed, this, &ABomberManUnit::InputMoveRightPressed);
			PlayerInputComponent->BindAction("P1_Up", EInputEvent::IE_Pressed, this, &ABomberManUnit::InputMoveUpPressed);
			PlayerInputComponent->BindAction("P1_Down", EInputEvent::IE_Pressed, this, &ABomberManUnit::InputMoveDownPressed);

			PlayerInputComponent->BindAction("P1_Left", EInputEvent::IE_Released, this, &ABomberManUnit::InputMoveLeftReleased);
			PlayerInputComponent->BindAction("P1_Right", EInputEvent::IE_Released, this, &ABomberManUnit::InputMoveRightReleased);
			PlayerInputComponent->BindAction("P1_Up", EInputEvent::IE_Released, this, &ABomberManUnit::InputMoveUpReleased);
			PlayerInputComponent->BindAction("P1_Down", EInputEvent::IE_Released, this, &ABomberManUnit::InputMoveDownReleased);

			PlayerInputComponent->BindAction("P1_Bomb", EInputEvent::IE_Pressed, this, &ABomberManUnit::InputBomb);
		}
		else if (PC->GetPlayerIndex() == 1)
		{
			PlayerInputComponent->BindAction("P2_Left", EInputEvent::IE_Pressed, this, &ABomberManUnit::InputMoveLeftPressed);
			PlayerInputComponent->BindAction("P2_Right", EInputEvent::IE_Pressed, this, &ABomberManUnit::InputMoveRightPressed);
			PlayerInputComponent->BindAction("P2_Up", EInputEvent::IE_Pressed, this, &ABomberManUnit::InputMoveUpPressed);
			PlayerInputComponent->BindAction("P2_Down", EInputEvent::IE_Pressed, this, &ABomberManUnit::InputMoveDownPressed);

			PlayerInputComponent->BindAction("P2_Left", EInputEvent::IE_Released, this, &ABomberManUnit::InputMoveLeftReleased);
			PlayerInputComponent->BindAction("P2_Right", EInputEvent::IE_Released, this, &ABomberManUnit::InputMoveRightReleased);
			PlayerInputComponent->BindAction("P2_Up", EInputEvent::IE_Released, this, &ABomberManUnit::InputMoveUpReleased);
			PlayerInputComponent->BindAction("P2_Down", EInputEvent::IE_Released, this, &ABomberManUnit::InputMoveDownReleased);

			PlayerInputComponent->BindAction("P2_Bomb", EInputEvent::IE_Pressed, this, &ABomberManUnit::InputBomb);
		}
		else
		{
			checkf(0, TEXT("Unhandled player"));
		}
	}

}

void ABomberManUnit::HandleMovementInput()
{
	FVector InputVector = FVector::ZeroVector;

	if (bMoveLeft | bMoveRight && !(bMoveLeft && bMoveRight))
	{
		if (bMoveLeft)
		{
			InputVector.Y = -1.0f;
		}
		else if (bMoveRight)
		{
			InputVector.Y = 1.0f;
		}
	}

	if (bMoveUp | bMoveDown && !(bMoveUp && bMoveDown))
	{
		if (bMoveUp)
		{
			InputVector.X = 1.0f;
		}
		else if (bMoveDown)
		{
			InputVector.X = -1.0f;
		}

	}

	GetCharacterMovement()->AddInputVector(InputVector);
}


void ABomberManUnit::InputMoveLeftPressed()
{
	bMoveLeft = true;
}

void ABomberManUnit::InputMoveRightPressed()
{
	bMoveRight = true;
}

void ABomberManUnit::InputMoveUpPressed()
{
	bMoveUp = true;
}

void ABomberManUnit::InputMoveDownPressed()
{
	bMoveDown = true;
}

void ABomberManUnit::InputMoveLeftReleased()
{
	bMoveLeft = false;
}

void ABomberManUnit::InputMoveRightReleased()
{
	bMoveRight = false;
}

void ABomberManUnit::InputMoveUpReleased()
{
	bMoveUp = false;
}

void ABomberManUnit::InputMoveDownReleased()
{
	bMoveDown = false;
}

void ABomberManUnit::InputBomb()
{
	ABomberManGameModeBase *GameMode = Cast<ABomberManGameModeBase>(UGameplayStatics::GetGameMode(this));

	if (RemoteDetonatorCount > 0 && Bombs.Num() > 0)
	{
		// Detonate all bombs, handle the case of having placed more than 1 bomb and then picking up detonator,
		// so we can blow mutliple bombs that were already placed.  Iterate in reverse
		for (int i = Bombs.Num() - 1; i >= 0; i--)
		{
			Bombs[i]->Explode();
		}
		return;
	}

	if (Bombs.Num() >= GetMaxBombs())
	{
		return;
	}

	if (GameMode)
	{
		int TileX, TileY;
		FVector TileCenter;
		FVector vStart = GetActorLocation();
		GameMode->Level->WorldCoordsToTileCoords(vStart + GetActorForwardVector()*TILESIZE, TileX, TileY);
		GameMode->Level->TileCoordsToWorldCoords(TileX, TileY, TileCenter);
		
		TileCenter.Z += 50.0f;

		FHitResult HitResult;
		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(this);



		bool bHit = GetWorld()->LineTraceSingleByChannel(HitResult, vStart, TileCenter, ECC_Bomb, QueryParams);

		FName CollisionProfileName;
		ECollisionResponse ECR;
		if (bHit)
		{
			CollisionProfileName = HitResult.GetComponent()->GetCollisionProfileName();
			ECR = HitResult.GetComponent()->GetCollisionResponseToChannel(ECC_Bomb);

		}

		if (!bHit)
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.bNoFail = true;

			ABomberManBomb *NewBomb = GetWorld()->SpawnActor<ABomberManBomb>(BombClass, TileCenter, FRotator::ZeroRotator, SpawnParams);
			NewBomb->OnBombExplode.AddUniqueDynamic(this, &ABomberManUnit::OnBombExplode);
			Bombs.Add(NewBomb);
		}
	}

}

float ABomberManUnit::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	GetMesh()->SetAllBodiesSimulatePhysics(true);
	GetMesh()->SetPhysicsBlendWeight(1.0f);
	GetMesh()->SetCollisionProfileName(FName("PhysicsActor"));

	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	FVector Impulse = GetActorLocation() - DamageCauser->GetActorLocation();
	Impulse.Z += 50;
	Impulse.Normalize();
	GetMesh()->AddImpulseAtLocation(Impulse*50000, DamageCauser->GetActorLocation());

	bDead = true;

	return DamageAmount;
}

int ABomberManUnit::GetMaxBombs()
{
	if (RemoteDetonatorCount >= 1)
	{
		return 1;
	}
	
	return MaxBombCount;
}

void ABomberManUnit::OnPowerUpExpired(ABomberManPickup* Pickup)
{
	Pickup->RemoveEffect(this);
}

void ABomberManUnit::ApplyPowerUp(ABomberManPickup* Pickup)
{
	Pickup->ApplyEffect(this);
}

void ABomberManUnit::OnBombExplode(ABomberManBomb *ExplodedBomb)
{
	Bombs.Remove(ExplodedBomb);
}