// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "DestructibleComponent.h"
#include "BomberManPickup.h"
#include "BomberManWall.generated.h"

UCLASS()
class BOMBERMAN_API ABomberManWall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABomberManWall();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	void SpawnPickup();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Wall)
	bool bDestructible;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Bomb)
	UBoxComponent *BoxComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Bomb)
	UDestructibleComponent *DestructibleComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pickups)
	TArray< TSubclassOf<ABomberManPickup> > PickupClasses;

	bool bDestroyed;
};
