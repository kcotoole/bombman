// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberManPlayerController.h"
#include "BomberManCamera.h"
#include "EngineUtils.h"
#include "Kismet/GameplayStatics.h"

ABomberManPlayerController::ABomberManPlayerController()
{
	bAutoManageActiveCameraTarget = false;
}

void ABomberManPlayerController::BeginPlay() 
{
	Super::BeginPlay();

	for (TActorIterator<ABomberManCamera>  ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		SetViewTarget(*ActorItr);
	}

	if (GetInputIndex() == 0)
	{
		// This is a pretty hacky spot to put this, TODO: Find a more appropriate place
		APlayerController *PC = UGameplayStatics::CreatePlayer(GetWorld(), 1, true);
	}
}

// Called to bind functionality to input
void ABomberManPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
}

int ABomberManPlayerController::GetPlayerIndex()
{
	return GetInputIndex();
}