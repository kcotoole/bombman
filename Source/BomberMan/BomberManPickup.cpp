// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberManPickup.h"
#include "BomberManLevel.h"


// Sets default values
ABomberManPickup::ABomberManPickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	BoxComp->SetBoxExtent(FVector(TILESIZE / 2, TILESIZE / 2, TILESIZE / 2), false);
	RootComponent = BoxComp;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PickupMesh"));
	MeshComp->SetupAttachment(BoxComp);

	// Add an overlap delegate so we can handle picking it up
	BoxComp->OnComponentBeginOverlap.AddDynamic(this, &ABomberManPickup::OnOverlap);

	bPickedUp = false;

	PowerUpTimeLeft = 10.0f;
}

// Called when the game starts or when spawned
void ABomberManPickup::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void ABomberManPickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bPickedUp)
	{
		if (PowerUpTimeLeft > 0.0f)
		{
			PowerUpTimeLeft -= DeltaTime;

			if (PowerUpTimeLeft <= 0.0f)
			{
				OnPowerUpExpired.Broadcast(this);
				Destroy();
			}
		}
	}

}

void ABomberManPickup::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	ABomberManUnit *Unit = Cast<ABomberManUnit>(OtherActor);

	if (Unit)
	{
		PickUp(Unit);
	}
}

void ABomberManPickup::PickUp(ABomberManUnit *PickupByUnit)
{
	if (!bPickedUp)
	{
		// When a powerup expires, call this delegate
		OnPowerUpExpired.AddUniqueDynamic(PickupByUnit, &ABomberManUnit::OnPowerUpExpired);

		PickupByUnit->ApplyPowerUp(this);

		SetActorHiddenInGame(true);
		MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		bPickedUp = true;
	}
}

void ABomberManPickup::ApplyEffect_Implementation(ABomberManUnit *Unit)
{

}

void ABomberManPickup::RemoveEffect_Implementation(ABomberManUnit *Unit)
{

}

float ABomberManPickup::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	Destroy();

	return DamageAmount;
}