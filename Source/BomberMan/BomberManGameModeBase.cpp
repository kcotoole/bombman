// Fill out your copyright notice in the Description page of Project Settings.

#include "BomberManGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "BomberManUnit.h"

ABomberManGameModeBase::ABomberManGameModeBase()
{
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;

	RoundTime = 10.0f;
}

void ABomberManGameModeBase::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	FActorSpawnParameters SpawnParams;
	SpawnParams.bNoFail = true;

	Level = GetWorld()->SpawnActor<ABomberManLevel>(BomberManLevelClass, FTransform::Identity, SpawnParams);
}

bool ABomberManGameModeBase::RoundShouldEnd()
{
	APlayerController *PC1 = UGameplayStatics::GetPlayerController(this, 0);
	APlayerController *PC2 = UGameplayStatics::GetPlayerController(this, 1);

	if (!PC1 || !PC2)
	{
		// Something is wrong, we don't have a player controller for one of the players
		// Fail gracefully
		return true;
	}

	ABomberManUnit *Unit_P1 = Cast<ABomberManUnit>(PC1->GetPawn());
	ABomberManUnit *Unit_P2 = Cast<ABomberManUnit>(PC2->GetPawn());

	if (!Unit_P1 || !Unit_P2)
	{
		// Something is wrong, we don't have a player pawn for one of the players
		// Fail gracefully
		return true;
	}

	if (RoundTime <= 0 || Unit_P1->IsDead() || Unit_P2->IsDead())
	{
		return true;
	}

	return false;
}

void ABomberManGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (RoundTime > 0.0f)
	{
		RoundTime -= DeltaTime;
	}

	if (RoundShouldEnd() && EndRoundWidget == NULL)
	{
		if (RoundTime < 0.0f)
		{
			RoundTime = 0.0f;
		}

		EndRound();
	}
}

void ABomberManGameModeBase::EndRound()
{
	APlayerController *PC1 = UGameplayStatics::GetPlayerController(this, 0);
	APlayerController *PC2 = UGameplayStatics::GetPlayerController(this, 1);

	if (!PC1 || !PC2)
	{
		// Something is wrong, we don't have a player controller for one of the players
		// Fail gracefully
		return;
	}

	ABomberManUnit *Unit_P1 = Cast<ABomberManUnit>(PC1->GetPawn());
	ABomberManUnit *Unit_P2 = Cast<ABomberManUnit>(PC2->GetPawn());

	if (!Unit_P1 || !Unit_P2)
	{
		// Something is wrong, we don't have a player pawn for one of the players
		// Fail gracefully
		return;
	}

	// Pause the game
	UGameplayStatics::SetGlobalTimeDilation(this, 0.1f);

	if (Unit_P1->IsDead() && Unit_P2->IsDead() || !(Unit_P1->IsDead() || Unit_P2->IsDead()))
	{
		RoundWinnerString = TEXT("DRAW");
	}
	else if (Unit_P1->IsDead())
	{
		RoundWinnerString = TEXT("Blue Player Wins!");
	}
	else if (Unit_P2->IsDead())
	{
		RoundWinnerString = TEXT("Red Player Wins!");
	}

	if (EndRoundWidgetClass)
	{
		EndRoundWidget = CreateWidget<UUserWidget>(PC1, EndRoundWidgetClass);
		EndRoundWidget->AddToViewport(1);
	}

	

	
}